define([], function () {
  'use strict';

  controller.$inject = ['authenticationService', '$window', '$location'];

  function controller(authenticationService, $window, $location) {
    var _this = this;
    _this.activeTab = $location.path();
    _this.logout = logout;
    _this.changeTab = changeTab;

    activate();

    function activate() {
      if (_this.activeTab === '/' || _this.activeTab === '') {
        _this.activeTab = '/checkin';
      }
    }

    function changeTab(tabName) {
      _this.activeTab = tabName;
    }

    function logout() {
      authenticationService.logout();
      $window.location.href = '/';
    }
  }

  return controller;
});
define([], function () {
  'use strict';

  checkinEndpointsService.$inject = ['$q','$timeout'];

  function checkinEndpointsService ($q,$timeout) {
    var es = this;

    es.getStatus = getStatus;
    es.timeIn = timeIn;
    es.timeOut = timeOut;
    ////////////////////

    function getStatus(payload){
      var p = $q.defer();
      $timeout(function() {
        p.resolve({
          timeInStatus:601
        });
      }, 1000);
      return p.promise;
    }

    function timeIn(payload){
      var p = $q.defer();
      $timeout(function() {
        p.resolve({
          status:603
        });
      }, 500);
      return p.promise;
    }

    function timeOut(payload){
      var p = $q.defer();
      $timeout(function() {
        p.resolve({
          status:703
        });
      }, 500);
      return p.promise;
    }
  }

  return checkinEndpointsService;
});

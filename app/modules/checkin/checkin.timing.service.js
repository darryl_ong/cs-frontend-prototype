define(['moment'], function(moment) {
  'use strict';

  checkinTimingService.$inject = ['$q', '$interval', 'checkinEndpointsService', 'authenticationService'];

  function checkinTimingService($q, $interval, endpointsService, authenticationService) {
    var service = this;
    var TYPE_ERROR = 'error';
    var TYPE_INFO = 'info';
    var TYPE_ASYNC = 'async';

    service.clock = new Date();
    service.status = {message:'', type:TYPE_INFO};
    service.tickInterval = 1000;
    service.startTime = undefined;
    service.endTime = undefined;
    service.runningTime = undefined;

    service.checkStatus = checkStatus;
    service.timeIn = timeIn;
    service.timeOut = timeOut;

    init();
    ////////////////////

    function init() {
      $interval(function () {
          service.clock = Date.now();
          service.runningTime = service.clock - service.startTime;
        }, service.tickInterval);
      checkStatus();
    }

    function checkStatus() {
      setStatus('Checking for status...', TYPE_ASYNC);
      endpointsService.getStatus(createPayload(service.clock)).then(function(body){
        if(body.timeInStatus === 600){
          service.startTime = moment(body.lastTimeIn,'HH:mm:ss').toDate();
          setStatus('RUNNING', TYPE_INFO);
        }else if(body.timeInStatus === 601 || body.timeInStatus === 700){
          setStatus('IDLE', TYPE_INFO);
        }else{
          setStatus('ERROR: ' + body.message, TYPE_ERROR);
        }
      },function(error){
        setStatus('SERVICE ERROR: ' + error, TYPE_ERROR);
      });
    }

    function timeIn() {
      service.startTime = service.clock;
      service.runningTime = undefined;
      setStatus('Timing in...', TYPE_ASYNC);
      endpointsService.timeIn(createPayload(service.startTime)).then(function(body){
        if(body.status === 603){
          setStatus('RUNNING', TYPE_INFO);
        }else{
          setStatus('ERROR: ' + body.message, TYPE_ERROR);
        }
      }, function(error){
        setStatus('SERVICE ERROR: ' + error, TYPE_ERROR);
      });
    }

    function timeOut() {
      service.endTime = service.clock;
      setStatus('Timing out...', TYPE_ASYNC);
      endpointsService.timeOut(createPayload(service.endTime)).then(function(body){
        if(body.status === 703){
          setStatus('IDLE', TYPE_INFO);
        }else{
          setStatus('ERROR: ' + body.message, TYPE_ERROR);
        }
      }, function(error){
        setStatus('SERVICE ERROR: ' + error, TYPE_ERROR);
      });
    }

    function createPayload(datetime){
      var payload = {
        employeeId: authenticationService.getId(),
        'employee_id': authenticationService.getId(), //FIXME: due to inconsistent backend
        date: moment(datetime).format("YYYY-MM-DD"),
        time: moment(datetime).format("HH:mm:ss")
      };
      return payload;
    }

    function setStatus(message,type){
      service.status.message = message;
      service.status.type = type ? type : TYPE_INFO;
    }
  }

  return checkinTimingService;
});

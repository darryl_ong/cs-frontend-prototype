define([], function () {
  'use strict';

  checkinEndpointsService.$inject = ['$q'];

  function checkinEndpointsService ($q) {
    var es = this;

    es.getStatus = getStatus;
    es.timeIn = timeIn;
    es.timeOut = timeOut;
    ////////////////////

    function getStatus(payload){
      var p = $q.defer();
      gapi.client.timekeeping.employees.timerecords.status.get(payload).then(function(response){
        p.resolve(JSON.parse(response.body));
      },function(response){
        p.reject("Service not found.");
      });
      return p.promise;
    }

    function timeIn(payload){
      var p = $q.defer();
      gapi.client.timekeeping.employees.timerecords.timein.insert(payload).then(function(response){
        p.resolve(JSON.parse(response.body));
  		},function(response){
        p.reject("Service not found.");
      });
  		return p.promise;
    }

    function timeOut(payload){
      var p = $q.defer();
      gapi.client.timekeeping.employees.timerecords.timeout.insert(payload).then(function(response){
        p.resolve(JSON.parse(response.body));
  		},function(response){
        p.reject("Service not found.");
      });
  		return p.promise;
    }
  }

  return checkinEndpointsService;
});

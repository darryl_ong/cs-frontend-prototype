/**
 * @ngdoc function
 * @name app.controller:SomeFeatureController
 * @description
 * # SomeFeatureController
 * Controller of the app
 */
define([], function() {
  'use strict';

  CheckinCoreController.$inject = ['checkinTimingService'];

  function CheckinCoreController(timingService) {
    var vm = this;
    vm.timingService = timingService;
  }

  return CheckinCoreController;
});

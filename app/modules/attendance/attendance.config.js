define(['require'], function(require) {
  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider
      .when('/attendance', {
        templateUrl: require.toUrl('./attendance.calendar.partial.html'),
        controller: 'AttendanceCoreController',
        controllerAs: 'ac',
      });
  }

  return config;
});

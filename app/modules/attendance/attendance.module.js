define([
  './attendance.config',
  './attendance.core.controller',
  'attendanceEndpoints',
  './attendance.records.service',
  'angular',
  'angular-route',
  'angular-bootstrap-calendar',
  'authentication/authentication.module'
], function(config, coreController, endpointsService, recordsService, angular) {
    var app = angular
      .module('attendanceModule', ['ngRoute','mwl.calendar'])
      .config(config)
      .controller('AttendanceCoreController', coreController)
      .service('attendanceEndpointsService', endpointsService)
      .service('attendanceRecordsService', recordsService);
});

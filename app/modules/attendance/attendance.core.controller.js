/**
 * @ngdoc function
 * @name AttendanceCoreController
 * @description
 * # AttendanceCoreController
 * Controller of the app
 */
define(['moment'], function(moment) {
  'use strict';

  AttendanceCoreController.$inject = ['attendanceRecordsService','authenticationService'];

  function AttendanceCoreController(attendanceRecordsService, authenticationService) {
    var vm = this;
    var TYPE_READY = 'ready';
    var TYPE_LOADING = 'loading';
    var TYPE_ERROR = 'error';

    vm.events = [];
    vm.calendarView = 'month';
    vm.calendarDay = new Date(); //initialize to current day

    vm.status = {message:'', type:TYPE_READY};
    vm.requestRecords = requestRecords;
    activate();

    //////
    function activate() {
      requestRecords();
    }

    function requestRecords() {
      setStatus('Retrieving records for the month...', TYPE_LOADING);
      attendanceRecordsService.getRecords(createPayload(vm.calendarDay)).then(function(eventList){
        vm.events = eventList;
        setStatus('Ready', TYPE_READY);
      },function(error){
        setStatus('ERROR: Unable to retrieve records. ' + error, TYPE_ERROR);
      });
    }

    function createPayload(day) {
      var payload = {
        employeeId: authenticationService.getId(),
        dateFrom: moment(day).startOf('month').format("YYYY-MM-DD"),
        dateTo: moment(day).endOf('month').format("YYYY-MM-DD")
      };
      return payload;
    }

    function setStatus(message,type){
      vm.status.message = message;
      vm.status.type = type ? type : TYPE_READY;
    }
  }

  return AttendanceCoreController;
});

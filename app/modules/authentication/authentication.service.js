define([], function () {
  'use strict';

  var service = function service($window, $cookies) {
    var _this = this;

    _this.logout = logout;
    _this.isAuthenticated = isAuthenticated;
    _this.auth = auth;
    _this.getId = getId;

    function auth(token) {
      $cookies.putObject('sessionId', token);
    }

    function logout() {
      $cookies.remove('sessionId');
    }

    function isAuthenticated() {
      if ($cookies.getObject('sessionId')) {
        return true;
      } else {
        return false;
      }
    }

    function getId() {
      return $cookies.getObject('sessionId');
    }

  };

  service.$inject = ['$window', '$cookies'];

  return service;
});
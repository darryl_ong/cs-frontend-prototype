define([], function () {
  'use strict';
  
  function loadingDirective() {
    return {
      scope: {
              expr:'='
            },
      restrict: 'E',
      templateUrl: 'modules/common/loading-page.partial.html'
    };
  }

  loadingDirective.$inject = [];

  return loadingDirective;
});
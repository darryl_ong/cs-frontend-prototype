define(['require'], function (require) {
  function config($routeProvider) {
    $routeProvider.when('/payroll', {
      templateUrl: require.toUrl('./payroll.html'),
      controller: 'PayrollController',
      controllerAs: 'pc',
      loginRequired: true,
      resolve: {
        payroll_details: ['payrollService', function (payrollService) {
          // Get user id here or some unique identification for the logged in user
          //var user_id = payrollService.getUserId();
          var result = payrollService.getAllPayrollDetails(1);

          return result;
        }]
      }
    });
  }

  config.$inject = ['$routeProvider'];

  return config;
});

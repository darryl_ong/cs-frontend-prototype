define([], function () {
  'use strict';
  var payrollService = function service($http, authenticationService, payrollEndpointService) {
    return {
      getAllPayrollDetails: function (user_id) {

        return payrollEndpointService.getAllPayrollDetails(user_id).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      getUserId: function () {
        return authenticationService.getId();
      }
    };
  };

  payrollService.$inject = ['$http', 'authenticationService', 'payrollEndpointService'];

  return payrollService;
});
define([
  'payroll/payroll.controller',
  'payroll/payroll.service',
  'convertDate-filter',
  'startFrom-filter',
  'camelCaseToSpaceDelimited-filter',
  'payroll/payroll.config',
  'payroll-endpoint-service',
  'authentication/authentication.module',
  'angular',
  'angular-route',
  'angular-bootstrap'], function (controller, service, convertDateFilter, startFromFilter, camelCaseToSpaceDelimitedFilter, config, payrollEndpointService) {
  angular.module('payrollModule', ['ngRoute', 'ui.bootstrap', 'authenticationModule'])
    .controller('PayrollController', controller)
    .factory('payrollService', service)
    .factory('payrollEndpointService', payrollEndpointService)
    .filter('convertDate', convertDateFilter)
    .filter('startFrom', startFromFilter)
    .filter('camelCaseToSpaceDelimited', camelCaseToSpaceDelimitedFilter)
    .config(config);
});

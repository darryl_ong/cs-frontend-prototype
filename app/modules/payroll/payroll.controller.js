define([], function () {
  'use strict';

  function payrollController(payroll_details,$timeout) {
    var vm = this;
    vm.ready = false;
    vm.currentPage = 1;
    vm.numPerPage = 8;
    vm.setActive = setActive;
    vm.items = payroll_details;
    vm.totalItems = vm.items.length;
    vm.noOfPages = 8;

    function setActive(index, data) {
      vm.ready = false;
      $timeout(function () {
        vm.selected = data;
        vm.currentIndex = index;
        vm.ready = true;
      }, 100);
    }
  }

  payrollController.$inject = ['payroll_details','$timeout'];
  return payrollController;
});

define([], function () {
  'use strict';

  contractEndpointService.$inject = ['$q'];

  function contractEndpointService ($q) {
    var _this = this;

    _this.getContractDetail = getContractDetail;

    function getContractDetail(customerId, contractId){
      var p = $q.defer();
      gapi.client.middleware.contract.get({'contractId' : contractId,
                                          'customerId' : customerId})
                                      .then(function(response){
                                        console.log("Success: " + response);
                                        p.resolve(response);
                                      },function(response){
                                        console.log("Fail: " + response);
                                        p.reject(response);
                                      });
  		return p.promise;
    }
  }

  return contractEndpointService;
});

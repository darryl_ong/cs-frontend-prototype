define([], function () {
  'use strict';

  contractEndpointService.$inject = ['$q', '$timeout'];
  
  function contractEndpointService ($q,$timeout) {
    var _this = this;
    _this.getContractDetail = getContractDetail;

    function generateContract(customerId, contractId){
      var contract = {
        "result" : {
           "summary": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean volutpat vel quam vitae venenatis.",
           "skillsList": [
            "Top Skill",
            "Weapons Management",
            "Self-defense Skills",
            "Language Skills"
           ],
           "serviceLine": {
            "name": "Service Line of Contract"
           },
           "accountList": [
            {
             "name": "Account_01",
             "assignment": {
              "name": "Account_01 Assignment"
             },
             "workOrderList": [
              {
               "type": "Deliver the Money to BPI"
              },
              {
               "type": "Deliver the Money to BDO"
              }
             ]
            }
           ],
           "kind": "contracts#contractsItem"
        } 
      };

      contract.result["id"] = contractId;
      contract.result["customerId"] = customerId;
      contract.result["title"] = "Contract " + customerId + "_" + contractId;
      
      return contract;
    }

    function getContractDetail(customerId, contractId){
      var p = $q.defer();
      $timeout(function() {
        p.resolve(generateContract(customerId, contractId));
      }, 1000);

  		return p.promise;
    }
  }

  return contractEndpointService;
});
define([], function () {
  'use strict';

  service.$inject = ['$http', 'contractEndpointService'];

  function service($http, contractEndpointService) {
    var _this = this;

    _this.getCustomerContractDetails = getCustomerContractDetails;

    function getCustomerContractDetails(customerId, contractId) {
        return contractEndpointService.getContractDetail(customerId, contractId).then(function (response) {
          return response.result;
        }, function(response){
          return response.result;
        });
    }
  }
   
  return service;
});
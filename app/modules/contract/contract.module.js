define([
'angular', 
'angular-route', 
'./contract.config', 
'./contract.controller', 
'./contract.service', 
'contractEndpoints'
], function (angular, angularRoute, config, controller, service, contractEndpointService) {
  'use strict';

  angular.module('contractModule', ['ngRoute'])
  		 .controller('ContractController', controller)
  		 .service('contractService', service)
  		 .service('contractEndpointService', contractEndpointService)
  		 .config(config);
});
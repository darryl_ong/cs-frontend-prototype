define([
  './registration.config',
  './registration.mock.endpoint.service',
  './registration.controller',
  './registration.newEmail.directive',
  'angular',
  'angular-route',
], function (config, service, controller, newEmailDirective) {
  'use strict';
  angular.module('registrationModule', ['ngRoute'])
    .config(config)
    .service('registrationEndpointService', service)
    .controller('RegistrationController', controller)
    .directive('newEmail', newEmailDirective);

});
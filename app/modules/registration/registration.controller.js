define([], function () {
  'use strict';

  function controller(registrationEndpointService) {
    var _this = this;
    _this.user = {};
    _this.position = '';
    _this.positions = ['slave','manager','tambay'];

    _this.register = function () {
      registrationEndpointService.register(_this.user);
    };

  }

  controller.$inject = ['registrationEndpointService'];

  return controller;
});
define([
'angular', 
'angular-route', 
'angular-ui-select', 
'angular-sanitize', 
'./customer.config', 
'./customer.controller', 
'./customer.service', 
'customerEndpoints',
'authentication/authentication.module',
'common/common.module'
], function (angular, angularRoute, angularUI, angularSanitize, config, controller, service, customerEndpointService) {
  'use strict';

  angular.module('customerModule', ['ngRoute', 'ui.select', 'ngSanitize', 'authenticationModule', 'commonModule'])
		  .controller('CustomerController', controller)
		  .service('customerService', service)
		  .service('customerEndpointService', customerEndpointService)
		  .config(config);
});
define([], function () {
  'use strict';
  
  service.$inject = ['$http','customerEndpointService'];
  
  function service($http, customerEndpointService) {
    var _this = this;

    _this.getCustomers = getCustomers;
    _this.getContracts = getContracts;
    _this.getCustomerById = getCustomerById;


    function getCustomers() {    
      return customerEndpointService.getCustomers().then(function (response) {
        return response.result;
      }, function (response) {
        return response.result;
      });
    }

   function getContracts(customerId) {
      return customerEndpointService.getContracts(customerId).then(function (response) {
        return response.result;
      }, function (error) {
        return error.result;
      });
    }

    function getCustomerById(customerId, customerList){
      var index;

      for (var i = 0; i <= customerList.length-1; i++) {
        if (customerList[i].id === customerId) {
            index= i;
            break;
        }
      }

      return index;
    }
  }

  return service;
});
define([], function () {
  'use strict';
  customerEndpointService.$inject = ['$q'];

  function customerEndpointService ($q) {
    var _this = this;

    _this.getCustomers = getCustomers;
    _this.getContracts = getContracts;

    var errorMessage = {"result":{
       "error": {
        "errors": [
         {
          "domain": "global",
          "reason": "backendError",
          "message": "503 Over Quota [Sample Error Message]"
         }
        ],
        "code": 503,
        "message": "503 Over Quota [Sample Error Message]"
       }
      }
    };

    function getCustomers(){
      var p = $q.defer();          
      p.reject(errorMessage);
      return p.promise;
    }

    function getContracts(customerId){
        var p = $q.defer();
        p.reject(errorMessage);
        return p.promise;
    }
  }

  return customerEndpointService;
});

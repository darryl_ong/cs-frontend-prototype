require.config({
  paths: {
    cryptojs: '//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256',
    angular: '../../bower_components/angular/angular',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    async: '../../bower_components/requirejs-plugins/src/async',
    jquery: '../../bower_components/jquery/dist/jquery',
    'angular-route': '../../bower_components/angular-route/angular-route',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
    'angular-mocks': '../../node_modules/angular-mocks/angular-mocks',
    'angular-ui-select': '../../bower_components/angular-ui-select/dist/select',
    'angular-sanitize': '../../bower_components/angular-sanitize/angular-sanitize',
    'convertDate-filter': 'common/convertDate.filter',
    'startFrom-filter': 'common/startFrom.filter',
    nprogress: '../../bower_components/nprogress/nprogress'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-ui-select': [
      'angular',
      'angular-sanitize'
    ],
    'angular-sanitize': {
      deps: [
        'angular'
      ]
    },
    'angular-route': [
      'angular'
    ],
    'angular-bootstrap': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    'angular-mocks': [
      'angular'
    ],
    'backend.mocks': [
      'angular-mocks'
    ],
    cryptojs: {
      exports: 'CryptoJS'
    }
  },
  packages: []
});
require([
  'nprogress',
  'bootstrap',
  'angular',
  'login/login.module',
  'async!https://apis.google.com/js/client.js!onload'
], function (NProgress) {
  'use strict';

  NProgress.configure({showSpinner: false});

  bootstrap();
  //Load Google APIs here
  function loadGoogleApis() {
    var apiList = [
      {name: 'middleware', version: 'v1', url: 'https://gae-modules-poc.appspot.com/_ah/api'}
    ];

    var apisToLoad = apiList.length;
    for (var i = 0; i < apiList.length; i++) {
      var api = apiList[i];
      NProgress.inc();
      console.log('Loading ' + api.name + '...');
      gapi.client.load(api.name, api.version, loadedApiCallback, api.url);
    }

    function loadedApiCallback() {
      if (--apisToLoad === 0) {
        NProgress.done();
        console.log("Endpoints Service is ready!");
      }
    }
  }

  function bootstrap() {
    console.log('Bootstrapping modules...');
    NProgress.inc();
    angular.bootstrap(document, ['loginModule']);
    loadGoogleApis();
    console.log('Modules bootstrapped!');
  }

});

define([
  'login/login.config',
  'login/login.controller',
  'login/login.mock.endpoint.service',
  'authentication/authentication.module',
  'angular',
  'angular-route'
], function (config, controller, loginService) {
  'use strict';

  var login = angular.module('loginModule', ['ngRoute', 'authenticationModule']);
  login.config(config);
  login.controller('LoginController', controller);
  login.service('loginEndpointService', loginService);
});
define(['require'], function (require) {
  'use strict';

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: require.toUrl('./login.html'),
      controller: 'LoginController',
      controllerAs: 'vm'
    }).otherwise({redirectTo: '/'});
  }

  return config;
});

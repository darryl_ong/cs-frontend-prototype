/*global module:false*/
module.exports = function (grunt) {
  var rewrite = require('connect-modrewrite');
  var globalConfig = {
    images: 'app/images',
    css: 'app/styles',
    fonts: 'app/fonts',
    scripts: 'app/modules',
    bower_path: 'bower_components',
    distribution: 'src/main/webapp'
  };
  // Project configuration.
  grunt.initConfig({
      globalConfig: globalConfig,
      clean: ['<%= globalConfig.distribution %>/fonts','<%= globalConfig.distribution %>/images','<%= globalConfig.distribution %>/modules','<%= globalConfig.distribution %>/styles','<%= globalConfig.distribution %>/*.html','<%= globalConfig.distribution %>/*.js', '.tmp', '<%= globalConfig.fonts %>/*.*', 'app/require.js', '<%= globalConfig.css %>/compiled-bootstrap', '<%= globalConfig.css %>/*.*'],
      jshint: {
        // http://jshint.com/docs/options/
        options: {
          curly: true,
          eqeqeq: true,             // http://stackoverflow.com/questions/359494/does-it-matter-which-equals-operator-vs-i-use-in-javascript-comparisons
          latedef: 'nofunc',        // Setting this option to "nofunc" will allow function declarations to be ignored
          // latedef_func: false,   // https://github.com/jshint/jshint/pull/942/files
          noarg: true,
          sub: true,
          undef: true,
          unused: false,
          validthis: true,
          shadow: true,
          boss: true,
          eqnull: true,
          funcscope: true,
          // in this section, we define the 3rd party global keywords used
          globals: {
            require: true,
            define: true,
            angular: true,
            console: true,
            describe: true,
            beforeEach: true,
            it: true,
            module: true,
            inject: true,
            spyOn: true,
            expect: true,
            jasmine: true,
            afterEach: true,
            xit: true,
            xdescribe: true,
            gapi: true,
            document: true,
            location: true
          }
        },
        allmodules: {
          src: [
            'Gruntfile.js',
            '<%= globalConfig.scripts %>/**/*.js'
          ]
        },
        alltests: {
          src: [
            'Gruntfile.js',
            'test/unit/**/*.js'
          ]
        }

      },
      customize_bootstrap: {
        task: {
          options: {
            bootstrapPath: '<%= globalConfig.bower_path %>/bootstrap',
            src: '<%= globalConfig.css %>/customized-bootstrap-less',
            dest: '<%= globalConfig.css %>/compiled-bootstrap',
          }
        }
      },
      less: {
        bootstrap: {
          src: '<%= customize_bootstrap.task.options.dest %>/bootstrap.less',
          dest: '<%= customize_bootstrap.task.options.dest %>/bootstrap.css'
        },
        theme: {
          src: '<%= customize_bootstrap.task.options.src %>/theme.less',
          dest: '<%= customize_bootstrap.task.options.dest %>/theme.css'
        },
        calendar: {
          src: '<%= customize_bootstrap.task.options.src %>/calendar.less',
          dest: '<%= customize_bootstrap.task.options.dest %>/calendar.css'
        }
      },
      requirejs: {
        main: {
          options: {
            baseUrl: '<%= globalConfig.scripts %>',
            mainConfigFile: '<%= globalConfig.scripts %>/main.js',
            name: 'main',
            out: '<%= globalConfig.distribution %>/modules/main.js',
            optimize: "uglify",
            preserveLicenseComments: false
          }
        },
        signin: {
          options: {
            baseUrl: '<%= globalConfig.scripts %>',
            mainConfigFile: '<%= globalConfig.scripts %>/signin-main.js',
            name: 'signin-main',
            out: '<%= globalConfig.distribution %>/modules/signin-main.js',
            optimize: "uglify",
            preserveLicenseComments: false
          }
        }
      },
      bowerRequirejs: {
        target: {
          rjsConfig: '<%= globalConfig.scripts %>/main.js',
          options: {
            transitive: true
          }
        }
      },
      copy: {
        pages: {
          files: [
            {
              expand: true,
              cwd: 'app',
              src: ['**/*.html'],
              dest: '<%= globalConfig.distribution %>/',
              filter: 'isFile'
            }
          ]
        },
        statics: {
          files: [
            {
              expand: true,
              cwd: 'app',
              src: ['fonts/**', 'images/**'],
              dest: '<%= globalConfig.distribution %>/',
              filter: 'isFile'
            }
          ]
        },
        require: {
          files: [
            {
              expand: true,
              cwd: 'app',
              src: ['require.js'],
              dest: '<%= globalConfig.distribution %>/',
              filter: 'isFile'
            }
          ]
        },
        init: {
          files: [
            {
              expand: true,
              flatten: true,
              src: ['node_modules/requirejs/require.js'],
              dest: 'app/',
              filter: 'isFile'
            },
            //{
            //  expand: true,
            //  flatten: true,
            //  src: ['bower_components/bootstrap/dist/css/bootstrap-theme.min.css'],
            //  dest: '<%= globalConfig.css %>/raw',
            //  filter: 'isFile'
            //},
            {
              expand: true,
              flatten: true,
              src: ['<%= globalConfig.bower_path %>/bootstrap/dist/fonts/*'],
              dest: '<%= globalConfig.fonts %>/',
              filter: 'isFile'
            },
            {
              expand: true,
              flatten: true,
              src: ['<%= globalConfig.bower_path %>/font-awesome/fonts/*'],
              dest: '<%= globalConfig.fonts %>/',
              filter: 'isFile'
            }
          ]
        }
      },
      useminPrepare: {
        html: ['app/**/*.html'],
        options: {
          dest: '<%= globalConfig.distribution %>'
        }
      },
      usemin: {
        html: ['<%= globalConfig.distribution %>/**/*.html'],
        options: {
          assetsDirs: ['<%= globalConfig.distribution %>']
        }
      },
      filerev: {
        dist: {
          src: [
            '<%= globalConfig.distribution %>/styles/**/*.css',
          ]
        }
      },
      karma: {
        unit: {
          configFile: 'karma.conf.js',
          singleRun: true
        }
      },
      uglify: {
        options: {
          mangle: true,
          compress: true,
          files: {
            '<%= globalConfig.distribution %>/modules/main.js': ['<%= requirejs.main.out %>'],
            '<%= globalConfig.distribution %>/modules/signin-main.js': ['<%= requirejs.signin.out %>']
          }
        }
      },
      connect: {
        options: {
          port: 9000,
          livereload: 35729,
          hostname: 'localhost' // * = accessible from anywhere ; default: localhost
        },
        livereload: {
          options: {
            open: true,
            keepAlive: true,
            base: '<%= globalConfig.distribution %>',
            middleware: function (connect, options) {

              var middleware = [
                connect().use(
                  '/bower_components',
                  connect.static('./bower_components')
                ),
                connect().use(
                  '/node_modules',
                  connect.static('./node_modules')
                ),
                connect().use(
                  '/login',
                  './app/index.html'
                ),
                connect.static('./app')
              ];

              return middleware;

            }
          }
        }
      },
      watch: {
        livereload: {
          files: [
            'app/modules/**/*.{js,html}',
            'app/styles/raw/*.css',
            'app/styles/customized-bootstrap-less/*.less',
            'bower.json'
          ],
          tasks: ['bowerRequirejs', 'test', 'compile-style']
        },
        jshint: {
          files: [
            '<%= globalConfig.scripts %>/**/*.js',
            'test/unit/**/*.js'
          ],
          tasks: 'jshint'
        }
      }
    }
  );

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-bower-requirejs');
  grunt.loadNpmTasks('grunt-customize-bootstrap');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-usemin');


  grunt.registerTask('init', ['clean', 'copy:init', 'bowerRequirejs', 'compile-style']);
  grunt.registerTask('compile-style', ['customize_bootstrap', 'less']);
  grunt.registerTask('serve', ['test', 'connect', 'watch']);
  grunt.registerTask('test', ['init', 'karma', 'jshint']);
  grunt.registerTask('release', ['test', 'requirejs', 'copy:pages', 'copy:require', 'copy:statics', 'build-pages']);
  grunt.registerTask('build-pages', ['useminPrepare', 'concat:generated', 'cssmin:generated', 'filerev', 'usemin']);

};

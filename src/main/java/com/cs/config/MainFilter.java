package com.cs.config;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author RMPader
 */
public class MainFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String target = request.getContextPath() + "/#" + request.getRequestURI();
        if(request.getQueryString() != null){
            target += "?" + request.getQueryString();
        }
        response.sendRedirect(target);

    }

    @Override
    public void destroy() {

    }

}
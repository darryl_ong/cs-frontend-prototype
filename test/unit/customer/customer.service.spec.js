define(['angular', 'angular-mocks', 'customer/customer.module'], function (notepad) {
  describe('Customer Service', function () {
    'use strict';

    var httpBackend;
    var customerService;
    var customerEndpointService;
    var q;

    var custList = {
      "result": {
        "items" : [{
          id : 1,
          name: 'cust#1'
        }, {
          id : 2,
          name: 'cust#2'
        }, {
          id : 3,        
          name: 'cust#3'
        }, {
          id : 4,        
          name: 'cust#4'
        }]
      }
    };

    beforeEach(module('customerModule'));

    beforeEach(inject(function ($injector) {
      customerService = $injector.get('customerService');
      httpBackend = $injector.get('$httpBackend');
      customerEndpointService = $injector.get('customerEndpointService');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on getCustomers()', function () {
      
      spyOn(customerEndpointService, 'getCustomers').and.returnValue(
        q.when(custList));

      customerService.getCustomers().then(function (response) {
        expect(response).toEqual(custList.result);
      });
    });

    it('should do an HTTP GET on getContracts()', function () {
      var returnContractObject = {
          "result" : {
              "items": [{
                'id': 1,
                'title': 'Contract #1',
                'summary': 'Summary of contract 1'
              }, {
                'id': 2,
                'title': 'Contract #2',
                'summary': 'Summary of contract 2'
              }]
            }
      };
      
      spyOn(customerEndpointService, 'getContracts').and.returnValue(
        q.when(returnContractObject));

      customerService.getContracts(4).then(function (response) {
        expect(response).toEqual(returnContractObject.result);
        expect(response.items[1].title).toEqual('Contract #2');
      });
    });

    it('should do an HTTP GET on getContracts() and return error', function () {
       var errorBody = {
        "result": {
             "error": {
              "errors": [
               {
                "domain": "global",
                "reason": "backendError",
                "message": ""
               }
              ],
              "code": 503,
              "message": ""
             }
            }
      };

      spyOn(customerEndpointService, 'getContracts').and.returnValue(
        q.when(errorBody));

      customerService.getContracts(5).then(function (response) {
        expect(response).toEqual(errorBody.result);
        expect(response.error.code).toEqual(503);
      });
    });

    it('should return the index of the given customerId', function () {

      expect(customerService.getCustomerById(4, custList.result.items)).toEqual(3);
      
    });

  });
});
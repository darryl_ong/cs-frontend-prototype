define(['angular', 'angular-mocks', 'customer/customer.module'], function () {
  describe('Customer Controller', function () {
    'use strict';

    var customerService;
    var customerController;

    var returnObject =  {"items": [{
          id : 1,
          name: 'cust#1'
        }, {
          id : 2,
          name: 'cust#2'
        }, {
          id : 3,
          name: 'cust#3'
        }, {
          id : 4,
          name: 'cust#4'
      }]};

    var returnContracts = {
      "items": [
        'contract1',
        'contract2',
        'contract3'
      ]
    };
    var customer = {
      id : 4,
      name: 'cust#4'
    };
    var scope;
    beforeEach(module('customerModule'));

    beforeEach(inject(function ($injector, $q, $controller, $rootScope) {
      customerService = $injector.get('customerService');
      scope = $rootScope.$new();
      spyOn(customerService, 'getCustomers').and.returnValue(
        $q.when(returnObject));

      spyOn(customerService, 'getContracts').and.returnValue(
        $q.when(returnContracts));

      spyOn(customerService, 'getCustomerById').and.returnValue(3);

      customerController = $controller('CustomerController', {
                                $routeParams : { customer_id: undefined,
                                                  page:1 },
                                $scope: scope
                            });
    }));

    it('should call the getCustomer of the service upon initalization', inject(function ($rootScope) {

      $rootScope.$digest();
      expect(customerController).toBeDefined();
      expect(customerController.customers).toEqual(returnObject);
      expect(customerController.customerId).toEqual(undefined);
      expect(customerService.getCustomers).toHaveBeenCalled();
      expect(customerService.getContracts).not.toHaveBeenCalled();
      expect(customerService.getCustomers.calls.count()).toEqual(1);
    }));

    it('should call the getContracts of the service upon on-select', inject(function ($rootScope) {

      customerController.getContracts(3, true);
      $rootScope.$digest();

      expect(customerController).toBeDefined();      
      expect(customerController.contracts).toEqual(returnContracts);
      expect(customerController.contracts.items.length).toEqual(3);
      expect(customerController.currentPage).toEqual(1);
      expect(customerController.contracts.items).toEqual(jasmine.arrayContaining(['contract3']));
      expect(customerService.getContracts).toHaveBeenCalledWith(3);
      expect(customerService.getContracts.calls.count()).toEqual(1);

    }));

    it('should call the getContracts of the service from activate', inject(function ($rootScope, $controller) {

      customerController = $controller('CustomerController', {
                                $routeParams : { customer_id: 4,
                                                  page:2 },
                                $scope: scope
                            });  
      $rootScope.$digest();

      expect(customerController).toBeDefined();      
      expect(customerController.contracts).toEqual(returnContracts);
      expect(customerController.contracts.items.length).toEqual(3);
      expect(customerController.currentPage).toEqual(2);
      expect(customerController.contracts.items).toEqual(jasmine.arrayContaining(['contract3']));
      expect(customerService.getContracts).toHaveBeenCalledWith(4);
      expect(customerService.getContracts.calls.count()).toEqual(1);

    }));

    it('should call the getCustomer and getContracts when called from contract-detail page and customer_id not undefined', inject(function ($q, $controller, $rootScope) {

      customerController = $controller('CustomerController', {
                                $routeParams : { customer_id: 4,
                                                  page:1 },
                                $scope: scope
                            });
      $rootScope.$digest();
      expect(customerController).toBeDefined();
      expect(customerController.customerId).toEqual(4);
      expect(customerController.selectedCustomer.selected).toEqual(customer);
      expect(customerController.customers).toEqual(returnObject);
      expect(customerController.contracts).toEqual(returnContracts);
      expect(customerService.getCustomers).toHaveBeenCalled();
      expect(customerService.getContracts).toHaveBeenCalledWith(4);
      expect(customerService.getCustomerById).toHaveBeenCalled();

    }));

    it('should call the getCustomer of the service when called from contract-detail page and customer_id is undefined', inject(function ($q, $controller, $rootScope) {

      customerController = $controller('CustomerController', {
                                $routeParams : { },
                                $scope: scope
                            });
      $rootScope.$digest();
      expect(customerController).toBeDefined();
      expect(customerController.customerId).toEqual(undefined);
      expect(customerController.customers).toEqual(returnObject);
      expect(customerController.contracts).toEqual([]);
      expect(customerController.currentPage).toEqual(1);
      expect(customerService.getCustomers).toHaveBeenCalled();
      expect(customerService.getContracts).not.toHaveBeenCalled();
      expect(customerService.getCustomerById).not.toHaveBeenCalled();

    }));

    it('should call the getCustomer of the service and return to second page', inject(function ($q, $controller, $rootScope) {

      customerController = $controller('CustomerController', {
                                $routeParams : { customer_id: 4,
                                                  page:2 },
                                $scope: scope
                            });
      $rootScope.$digest();
      expect(customerController).toBeDefined();
      expect(customerController.customerId).toEqual(4);
      expect(customerController.selectedCustomer.selected).toEqual(customer);
      expect(customerController.currentPage).toEqual(2);
      expect(customerController.contracts).toEqual(returnContracts);

    }));

    it('should route to the correct page', inject(function ($route) {

     expect($route.routes['/customer'].templateUrl).toMatch('modules/customer/customer.html');
     expect($route.routes['/customer'].controller).toEqual('CustomerController');

     expect($route.routes['/contracts'].templateUrl).toMatch('modules/customer/customer.html');
     expect($route.routes['/contracts'].controller).toEqual('CustomerController');

     expect($route.routes['/customer/:customer_id/:page'].templateUrl).toMatch('modules/customer/customer.html');
     expect($route.routes['/customer/:customer_id/:page'].controller).toEqual('CustomerController');
    }));

  });
});

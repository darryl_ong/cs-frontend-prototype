define(['moment','angular','angular-mocks','attendance/attendance.module'], function(moment) {
  'use strict';

  describe('AttendanceCoreController', function () {

    var attendanceCoreController, attendanceRecordsService, authenticationService, $rootScope, $q;

    // load the controller's module
    beforeEach(module('attendanceModule'));
    beforeEach(module('authenticationModule'));

    // Initialize the controller and mock interval
    beforeEach(inject(function ($controller,$injector){
      $rootScope = $injector.get('$rootScope');
      $q = $injector.get('$q');

      attendanceRecordsService = $injector.get('attendanceRecordsService');
      spyOn(attendanceRecordsService,'getRecords').and.returnValue($q.when([
        {}
      ]));

      attendanceCoreController = $controller('AttendanceCoreController');
    }));

    it('should have events, calendarView, and calendarDay, as required by the calendar directive', function() {
      expect(attendanceCoreController.events).toBeDefined();
      expect(attendanceCoreController.calendarView).toBeDefined();
      expect(attendanceCoreController.calendarDay).toBeDefined();
    });

    it('should send start and end dates of current month to the service', function() {
      $rootScope.$digest();
      expect(attendanceRecordsService.getRecords).toHaveBeenCalledWith(jasmine.objectContaining({
        dateFrom:moment(attendanceCoreController.calendarDay).startOf('month').format("YYYY-MM-DD"),
        dateTo:moment(attendanceCoreController.calendarDay).endOf('month').format("YYYY-MM-DD")
      }));
    });

  });
});

define(['angular', 'angular-mocks', 'contract/contract.module'], function (notepad) {
  describe('Contract Service', function () {
    'use strict';

    var httpBackend, contractService, contractEndpointService, q;

    beforeEach(module('contractModule'));

    beforeEach(inject(function ($injector) {
      contractService = $injector.get('contractService');
      contractEndpointService = $injector.get('contractEndpointService');
      httpBackend = $injector.get('$httpBackend');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on getCustomerContractDetails() and return data', function () {

        var contractDetail = {"result" : {
                "id": "5668600916475904",
                 "title": "Bank Transfer Contract v01",
                 "summary": "This is the summary of your contract. Place pertinent information here.",
                 "skillsList": [
                  "Weapons Handling Skill",
                  "Driving Skill",
                  "Self-defense Skill"
                 ],
                 "customerId": "4",
                 "accountList": [
                    {
                     "name": "Account Name 001",
                     "assignment": {
                        "name": "Bank Cash Transfer"
                     },
                     "workOrderList": [
                        {
                         "type": "Bank Transfer Order"
                        },
                        {
                         "type": "Another Bank Transfer Order"
                        }
                     ]
                    },
                    {
                     "name": "Account Name 002",
                     "assignment": {
                        "name": "Second Assignment Name"
                     }
                    }
                 ],
                 "kind": "contractapi#contractItem",
                 "etag": "\"29BVa5dTiL83IiqJapwNOqM3yuU/quACNgE53zQbNPHxzRhCKAcBAbA\""
              }
            };
      
      spyOn(contractEndpointService, 'getContractDetail').and.returnValue(
        q.when(contractDetail));

      contractService.getCustomerContractDetails(4, 5668600916475904).then(function (response) {
          expect(response).toEqual(contractDetail.result);
          expect(response.accountList[0].name).toEqual('Account Name 001');
      });
    });

    it('should do an HTTP GET on getCustomerContractDetails() and return error', function () {
      var errorBody = {
        "result": {
             "error": {
              "errors": [
               {
                "domain": "global",
                "reason": "backendError",
                "message": ""
               }
              ],
              "code": 503,
              "message": ""
             }
            }
      };
      spyOn(contractEndpointService, 'getContractDetail').and.returnValue(
        q.when(errorBody));

      contractService.getCustomerContractDetails(12345, 5629499534213121).then(function (response) {
        expect(response).toEqual(errorBody.result);
        expect(response.error.code).toEqual(503);
      });
      
    });

  });
});
define(['angular', 'angular-mocks', 'login/login.module'], function () {
  describe("Login Controller", function () {
    var $window;

    beforeEach(function () {
      module('loginModule');

      $window = {location: {replace: jasmine.createSpy()}};

      module(function ($provide) {
        $provide.value('$window', $window);
      });
    });

    it("should call the service login function", inject(function ($injector, $controller, $q, $rootScope) {
      var authService = $injector.get('authenticationService');
      spyOn(authService, 'auth');

      var loginService = $injector.get('loginEndpointService');
      spyOn(loginService, 'login').and.callFake(function () {
        authService.auth(1);
        return $q.when({data: {id: 1231}});
      });

      var controller = $controller('LoginController', {$routeParams: {next: '/next'}});
      controller.loginEmail = 'email';
      controller.password = 'unencrypted';
      controller.loginUser();

      $rootScope.$digest();
      expect(loginService.login).toHaveBeenCalledWith('email', 'bc276c3b995088c08cf933c43657bd73854864ae75168aa777159bcf3f882a6d');
    }));

    it("should call the service login function but not redirect to home when no session has been created", inject(function ($injector, $controller, $q, $rootScope) {
      var authService = $injector.get('authenticationService');
      spyOn(authService, 'auth');
      spyOn(authService, 'logout');
      
      var loginService = $injector.get('loginEndpointService');
      spyOn(loginService, 'login').and.callFake(function () {
        return $q.when({});
      });

      var controller = $controller('LoginController', {$routeParams: {next: '/next'}});
      controller.loginEmail = 'email';
      controller.password = 'unencrypted';
      controller.loginUser();

      $rootScope.$digest();
      expect(loginService.login).toHaveBeenCalledWith('email', 'bc276c3b995088c08cf933c43657bd73854864ae75168aa777159bcf3f882a6d');
    }));
  });
});
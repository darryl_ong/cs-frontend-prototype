define(['angular', 'angular-mocks', 'payroll/payroll.module'], function () {
  describe('Payroll Controller', function () {
    'use strict';
    var payrollCtrl, payrollService, scope,$controller;

    var payrollDetails = [{
          "employeeKey": "1",
          "dailyRate": 3181.82,
          "monthlyRate": 0.0,
          "hourlyRate": 0.0,
          "grossSalary": 57670.490000000005,
          "netSalary": 37067.7132,
          "taxRate": 0.32,
          "overtimePay": 397.73,
          "gasAllowance": 1000.0,
          "communicationAllowance": 1799.0,
          "riceAllowance": 1000.0,
          "clothingAllowance": 1500.0,
          "dateTo": "2015-08-08T00:00:00.000Z",
          "dateFrom": "2015-07-07T00:00:00.000Z",
          "deductions":
          {
            "incomeTaxDeduction": 18454.556800000002,
            "sssDeduction": 1024.22,
            "philHealthDeduction": 1024.0,
            "pagibigDeduction": 100.0,
            "absencesDeduction": 0.0,
            "totalDeduction": 20602.776800000003
          },
          "kind": "payroll#salariesItem"
      }];

    beforeEach(module('payrollModule'));

    beforeEach(inject(function ($injector, $rootScope, $q) {
      scope = $rootScope.$new();
      payrollService = $injector.get('payrollService');
      $controller = $injector.get('$controller');

      spyOn(payrollService, 'getAllPayrollDetails').and.returnValue(
          $q.when(payrollDetails));

      payrollCtrl = $controller('PayrollController', {
        $scope: scope,
        payroll_details: payrollDetails
      });

    }));

    it('Should define payroll controller with setActive() function', function(){
      expect(payrollCtrl).toBeDefined();
      expect(payrollCtrl.setActive).toBeDefined();
    });

    it('Should have initialized data needed for payroll', function () {
      expect(payrollCtrl.currentPage).toBe(1);
      expect(payrollCtrl.numPerPage).toBe(8);
      expect(payrollCtrl.items).toEqual(payrollDetails);
      expect(payrollCtrl.totalItems).toEqual(payrollDetails.length);
    });

  });
});
